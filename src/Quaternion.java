import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

    public static final double DELTA = 0.000001;

    private double realp;
    private double imapi;
    private double imapj;
    private double imapk;

    /** Constructor from four double values.
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion (double a, double b, double c, double d) {
        realp = a;
        imapi = b;
        imapj = c;
        imapk = d;
    }

    /** Real part of the quaternion.
     * @return real part
     */
    public double getRpart() {
        return realp;
    }

    /** Imaginary part i of the quaternion.
     * @return imaginary part i
     */
    public double getIpart() {
        return imapi;
    }

    /** Imaginary part j of the quaternion.
     * @return imaginary part j
     */
    public double getJpart() {
        return imapj;
    }

    /** Imaginary part k of the quaternion.
     * @return imaginary part k
     */
    public double getKpart() {
        return imapk;
    }

    /** Conversion of the quaternion to the string.
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        String i = "";
        String j = "";
        String k = "";

        String rp = "";
        String imi = "";
        String imj = "";
        String imk = "";

        if (imapi > DELTA) {
            i = "+";
        }
        if (imapj > DELTA) {
            j = "+";
        }
        if (imapk > DELTA) {
            k = "+";
        }

        rp = String.valueOf(realp);

        String a = "";
        if (Math.abs(this.realp) > DELTA){
            a += rp;
        } else {
            i = "";
        }
        imi = i + imapi + "i";
        if (Math.abs(this.imapi) > DELTA){
            a += imi;
        } else if (i.equals("") && Math.abs(this.realp) < DELTA) {
            j = "";
        }
        imj = j + imapj + "j";
        if (Math.abs(this.imapj) > DELTA){
            a += imj;
        }else if (i.equals("") && j.equals("") &&
                Math.abs(this.realp) < DELTA &&
                Math.abs(this.imapi) < DELTA) {
            k = "";
        }
        imk = k + imapk + "k";
        if (Math.abs(this.imapk) > DELTA){
            a += imk;
        }
        return a;

    }
    public static boolean checkIfNumber(String a){
        try{
            Double.parseDouble(a);
        } catch (RuntimeException E){
            return false;
        }
        return true;
    }

    /** Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     * @throws IllegalArgumentException if string s does not represent
     *     a quaternion (defined by the <code>toString</code> method)
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     */
    public static Quaternion valueOf (String s) {
        double newrealp = 0.0;
        double newrimagi = 0.0;
        double newrimagj = 0.0;
        double newrimagk = 0.0;
        s = s.replaceAll("[+]", " ").trim();
        String[] a = s.replaceAll("([-]?[0-9]+(?:\\.[0-9]*)?(?:[ijk]*)?)", "$0:").trim().split(":");
        for (int i = 0; i < a.length; i++) {
            if (checkIfNumber(a[i])) {
                newrealp = Double.parseDouble(a[i]);
            }
            if (a[i].endsWith("i")) {
                if (newrimagi > DELTA) {
                    throw new IllegalArgumentException("There can't be multible imaginari parts i");
                }
                try {
                    newrimagi = Double.parseDouble(a[i].replaceAll("[i]", ""));
                } catch (NumberFormatException e1) {
                    throw new NumberFormatException("String <" + s + "> does not represent a quaternion.(Imaginari part i error.)");
                }
            }
            if (a[i].endsWith("j")) {
                if (newrimagj > DELTA) {
                    throw new IllegalArgumentException("There can't be multible imaginari parts j");
                }
                try {
                    newrimagj = Double.parseDouble(a[i].replaceAll("[j]", ""));
                } catch (NumberFormatException e1) {
                    throw new NumberFormatException("String <" + s + "> does not represent a quaternion.(Imaginari part j error.)");
                }
            }
            if (a[i].endsWith("k")) {
                if (newrimagk > DELTA) {
                    throw new IllegalArgumentException("There can't be multible imaginari parts k");
                }
                try {
                    newrimagk = Double.parseDouble(a[i].replaceAll("[k]", ""));
                } catch (NumberFormatException e1) {
                    throw new NumberFormatException("String <" + s + "> does not represent a quaternion.(Imaginari part k error.)");
                }
            }
        }
        return new Quaternion (newrealp, newrimagi, newrimagj, newrimagk);
    }

    /** Clone of the quaternion.
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion (realp, imapi, imapj, imapk);
    }

    /** Test whether the quaternion is zero.
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return (Math.abs(this.realp) < DELTA) &&
                (Math.abs(this.imapi) < DELTA) &&
                (Math.abs(this.imapj) < DELTA) &&
                (Math.abs(this.imapk) < DELTA);
    }

    /** Conjugate of the quaternion. Expressed by the formula
     *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        double b = imapi;
        double c = imapj;
        double d = imapk;
        if (imapi > 0) {
            b = 0 - imapi;
        } else if ( imapi < 0) {
            b = Math.abs(imapi);
        }
        if (imapj > 0) {
            c = 0 - imapj;
        } else if ( imapj < 0) {
            c = Math.abs(imapj);
        }
        if (imapk > 0) {
            d = 0 - imapk;
        } else if ( imapk < 0) {
            d = Math.abs(imapk);
        }
        return new Quaternion (realp, b, c, d);
    }

    /** Opposite of the quaternion. Expressed by the formula
     *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        Quaternion new1 = this.conjugate();
        double newreal = realp;
        if (realp > 0) {
            newreal = 0 - realp;
        } else if ( realp < 0) {
            newreal = Math.abs(realp);
        }
        return new Quaternion (newreal, new1.imapi, new1.imapj, new1.imapk);
    }

    /** Sum of quaternions. Expressed by the formula
     *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus (Quaternion q) {
        return new Quaternion (this.realp + q.realp, this.imapi + q.imapi, this.imapj + q.imapj, this.imapk + q.imapk);
    }

    /** Product of quaternions. Expressed by the formula
     *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times (Quaternion q) {
        double newa = (this.realp * q.realp - this.imapi * q.imapi - this.imapj * q.imapj - this.imapk * q.imapk);
        double newb = (this.realp * q.imapi + this.imapi * q.realp + this.imapj * q.imapk - this.imapk * q.imapj);
        double newc = (this.realp * q.imapj - this.imapi * q.imapk + this.imapj * q.realp + this.imapk * q.imapi);
        double newd = (this.realp * q.imapk + this.imapi * q.imapj - this.imapj * q.imapi + this.imapk * q.realp);
        return new Quaternion (newa, newb, newc, newd);
    }

    /** Multiplication by a coefficient.
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times (double r) {
        return new Quaternion (this.realp * r, this.imapi * r, this.imapj * r, this.imapk * r);
    }

    /** Inverse of the quaternion. Expressed by the formula
     *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        double under = this.realp * this.realp + this.imapi * this.imapi + this.imapj * this.imapj + this.imapk * this.imapk;
        if (Math.abs(under) < DELTA) {
            throw new RuntimeException("Quaternion cannot be inverted. Division by zero!");
        }
        Quaternion f = this.conjugate();
        return new Quaternion (f.realp / under, f.imapi / under, f.imapj / under, f.imapk / under);
    }

    /** Difference of quaternions. Expressed as addition to the opposite.
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus (Quaternion q) {
        return new Quaternion (this.realp - q.realp, this.imapi - q.imapi, this.imapj - q.imapj, this.imapk - q.imapk);

    }

    /** Right quotient of quaternions. Expressed as multiplication to the inverse.
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight (Quaternion q) {
        try {
            return this.times(q.inverse());
        } catch (RuntimeException e1) {
            throw new RuntimeException("Quaternion cannot be divided by given quaternion. Division by zero when inverting divider!");
        }
    }

    /** Left quotient of quaternions.
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft (Quaternion q) {
        try {
            return q.inverse().times(this);
        } catch (RuntimeException e1) {
            throw new RuntimeException("Quaternion cannot be divided. Division by zero when inverting divisible!");
        }
    }

    /** Equality test of quaternions. Difference of equal numbers
     *     is (close to) zero.
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals (Object qo) {
        if (qo == null) {
            return false;
        }
        if(!(qo instanceof Quaternion)){
            return false;
        }
        final Quaternion q = (Quaternion) qo;

        return (Math.abs(this.realp - q.getRpart()) < DELTA) &&
                (Math.abs(this.imapi - q.getIpart()) < DELTA) &&
                (Math.abs(this.imapj - q.getJpart()) < DELTA) &&
                (Math.abs(this.imapk - q.getKpart()) < DELTA);
    }

    /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult (Quaternion q) {
        return (this.times(q.conjugate()).plus(q.times(this.conjugate()))).times(0.5);
    }

    /** Integer hashCode has to be the same for equal objects.
     * @return hashcode
     */
    @Override
    public int hashCode() {
        //Kasutatud allikas: https://stackoverflow.com/questions/10164546/correct-implementation-of-hashcode
        final int prime = 31;
        int result = 1;
        result = prime * result + Double.valueOf(realp).hashCode();
        result = prime * result + Double.valueOf(imapi).hashCode();
        result = prime * result + Double.valueOf(imapj).hashCode();
        result = prime * result + Double.valueOf(imapk).hashCode();
        return result;
    }

    /** Norm of the quaternion. Expressed by the formula
     *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(realp*realp+imapi*imapi+imapj*imapj+imapk*imapk);
    }

    /** Main method for testing purposes.
     * @param arg command line parameters
     */
    public static void main (String[] arg) {
        Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
        if (arg.length > 0)
            arv1 = valueOf (arg[0]);
        System.out.println ("first: " + arv1.toString());
        System.out.println ("real: " + arv1.getRpart());
        System.out.println ("imagi: " + arv1.getIpart());
        System.out.println ("imagj: " + arv1.getJpart());
        System.out.println ("imagk: " + arv1.getKpart());
        System.out.println ("isZero: " + arv1.isZero());
        System.out.println ("conjugate: " + arv1.conjugate());
        System.out.println ("opposite: " + arv1.opposite());
        System.out.println ("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion)arv1.clone();
        } catch (CloneNotSupportedException e) {};
        System.out.println ("clone equals to original: " + res.equals (arv1));
        System.out.println ("clone is not the same object: " + (res!=arv1));
        System.out.println ("hashCode: " + res.hashCode());
        res = valueOf (arv1.toString());
        System.out.println ("string conversion equals to original: "
                + res.equals (arv1));
        Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf (arg[1]);
        System.out.println ("second: " + arv2.toString());
        System.out.println ("hashCode: " + arv2.hashCode());
        System.out.println ("equals: " + arv1.equals (arv2));
        res = arv1.plus (arv2);
        System.out.println ("plus: " + res);
        System.out.println ("times: " + arv1.times (arv2));
        System.out.println ("minus: " + arv1.minus (arv2));
        double mm = arv1.norm();
        System.out.println ("norm: " + mm);
        System.out.println ("inverse: " + arv1.inverse());
        System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
        System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
        System.out.println ("dotMult: " + arv1.dotMult (arv2));
        System.out.println("ValueOF:" + Quaternion.valueOf("3-3.0-3.0-4.0k"));
    }
}
// end of file